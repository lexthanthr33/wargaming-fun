def decypher(s):
    '''
    By playing around with the encrypt function we can see that it uses
    two pieces of data to encrypt a string:
        1. the character position (0 indexed)
        2. the ascii value of the character
    encrypted_c = c_position + c_ascii (pseudo code speaking)
    So therefore we do the following operations to decrypt a string
        1. for each c in s -> get its ascii value (ord) 
           - Technically this is getting unicode data but all ascii chars are unicode
        2. get the position of each byte (enumerate)
        3. subtract position from the ascii value (inverse of encryption step)
        4. turn these bytes into a bytearray (useful for viewing)
        5. decode the byte array as ascii to see the original password!
        
    This is all done using generator expressions that hopefully arent too awful!
    '''
    return bytearray(b-i for i, b in enumerate(ord(c) for c in s)).decode('ascii')

if __name__ == '__main__':
    import sys
    print(decypher(sys.argv[1]))
