#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    for (size_t i = 0; argv[1][i+1]; i++, argv[1][i]-=i) ;
    printf("%s\n", argv[1]);
    return 0;
}
